const HDWalletProvider = require("@truffle/hdwallet-provider");
const Web3 = require("web3");
const provider = new HDWalletProvider(process.env.MNEMONIC, `https://ropsten.infura.io/v3/${process.env.INFURA_API_KEY}`);
const web3 = new Web3(provider);

const runDapp = require('./RunDapp.json');
const Contract = require('web3-eth-contract');
Contract.setProvider(provider);
const contract = new Contract(runDapp.abi, process.env.CONTRACT);

const winston = require('winston');
const {LoggingWinston} = require('@google-cloud/logging-winston');
const loggingWinston = new LoggingWinston();
const logger = winston.createLogger({
    level: 'info',
    transports: [
        new winston.transports.Console(),
        loggingWinston,
    ],
});

exports.withdraw = (message, context) => {

    let payload = Buffer.from(message.data, 'base64').toString();
    payload = JSON.parse(payload);
    logger.info(payload);

    const data = contract.methods.withdraw(payload.value).encodeABI();

    web3.eth.sendTransaction({
        from: process.env.RECEIVER,
        to: process.env.CONTRACT,
        data: data
    })
    .on('receipt', function(receipt) {
        logger.info(receipt);
    })
    .on('error', logger.error);
  
    provider.engine.stop();
};
